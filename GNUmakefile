# ruokin-vue is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ruokin-vue is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ruokin-vue. If not, see <https://www.gnu.org/licenses/>.

# DEPENDENCIES: podman, gawk, fzf, guix.

# LOAD ENV FILES
-include envs/.env.*

.DEFAULT_GOAL := test

RUNNER ?= podman
POD_NAME := ruokin
NAME := ruokin-vue
VERSION := $(shell awk '/version/ {line=substr($$2, 2,5); print line}' ./package.json)
VUE_IMAGE=${GITLAB_REGISTRY}/${USER}/${NAME}:${VERSION}
BACKEND_FOLDER=/app

# ========================================== PORCELAIN

.PHONY: up
up: image.start

.PHONY: down
down:

# ========================================== IMAGE

.PHONY: image.prod
image.prod:
	${RUNNER} container rm -f ${NAME}-prod
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-prod \
		--volume ${PWD}:/app \
		--workdir /app \
		${VUE_IMAGE}

.PHONY: image.start
image.start:
	${RUNNER} container rm -f ${NAME}-start
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-start \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		${VUE_IMAGE} \
		bash -c 'bun run dev'

.PHONY: image.test
image.test:
	${RUNNER} container rm -f ${NAME}-test
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-test \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		${VUE_IMAGE} \
		bash -c 'bun run test'

.PHONY: image.repl
image.repl:
	${RUNNER} container rm -f ${NAME}-repl
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-repl \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		${VUE_IMAGE} \
		bash

.PHONY: image.commands
image.commands:
	${RUNNER} container rm -f ${NAME}-commands
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-command \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		${VUE_IMAGE} \
		bash -c 'bun $(shell cat runtime-commands | fzf)'

.PHONY: image.build
image.build:
	${RUNNER} build \
		--file ./Dockerfile \
		--tag ${VUE_IMAGE}

.PHONY: image.publish
image.publish:
	${RUNNER} push ${VUE_IMAGE}

# ========================================== LOCAL
